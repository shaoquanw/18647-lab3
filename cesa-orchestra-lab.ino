#include <Assert.h>
#include <PowerDue.h>
#include <FreeRTOS_ARM.h>
#include <IPAddress.h>
#include <PowerDueWiFi.h>
#include "MIDIParser.h"
#include "WM_synth.h"

#include "ntp.h"
#include "DueClock.h"

#define WIFI_SSID "PowerDue"
#define WIFI_PASS "powerdue"

#define NTP_VERBOSE 1
#define MIDI_VERBOSE 0
#define CLK_VERBOSE 0

#define MIDI_PORT 4000
#define INET_LEN 16
#define BUFSIZE 128
#define NTP_MAX_DELAY 3000
#define NTP_MAX_OFFSET 300

#define IMPOSED_OFFSET (0 * 1000)
#define NTP_INTERVAL (1000 * 15)  // 15 second intervals?

uint32_t interval;
uint16_t adjust = 1;

MidiCallbacks_t midiCallbacks;
uint32_t _bpm = 120;
uint16_t _ticksPerNote = 96;
uint32_t note_count = 0;
uint32_t error_count = 0;
uint32_t error_total = 0;

/*
 * MIDI Related Functions
 */

void midi_handleEvent(uint16_t type, uint32_t delayTicks, uint8_t status, uint8_t note, uint8_t velocity, uint32_t instrument){
  switch(type){
    case MIDI_FILETYPE_REGULAR_DELTA:
      vTaskDelay(pdMS_TO_TICKS(TRACK_TIMER_PERIOD_MS(_ticksPerNote, _bpm) * delayTicks));
      break;
    case MIDI_FILETYPE_ABSOLUTE_DELTA: {
      // we need to handle this event only if actual time has passed delayTicks

      // how do we do that?

      // delayTicks is actual time in milliseconds
      // warning: MIDI time can only handle up to 32 bits (uint32_t) while actual time is 64 bits
      // only compare the lower 32 bits, assuming that MIDI time has wrapped around.
      uint64_t local_t = Clock.millis();
      uint32_t local_t_32 = (uint32_t)local_t;

      if(CLK_VERBOSE)
      {
          SerialUSB.print("Current: ");
          SerialUSB.print(local_t_32);
          SerialUSB.print("ms. Requested: ");
          SerialUSB.print(delayTicks);
          SerialUSB.print("ms. Difference: ");
      }
      
      if(local_t_32 < delayTicks)
      {
        uint32_t diff = delayTicks - local_t_32;
        if(CLK_VERBOSE)
        {
          SerialUSB.print(diff);
          SerialUSB.println("ms.\n");
        }
        vTaskDelay(pdMS_TO_TICKS(diff));
      }
      else if(local_t_32 > delayTicks)
      {
        if(CLK_VERBOSE)
        {
          SerialUSB.println("negative.\n");
        }
      }
      else
      {
        if(CLK_VERBOSE)
        {
          SerialUSB.println("0.\n");
        }
      }

      note_count++;
      
      local_t = Clock.millis();
      int64_t error = (int64_t)local_t - (int64_t)delayTicks;

      if(abs((int32_t)error) > 0)
      {
          SerialUSB.print("error: ");
          SerialUSB.print((int32_t)error);
          SerialUSB.println("ms.");
          error_count++;
          error_total += abs((int32_t)error);
      }

      break;
    }

    default:
      break;
  }


  
  
  if(status == STATUS_NOTE_ON && velocity > 0){
    WMSynth.playToneN(note, velocity, instrument);
  } else {
    WMSynth.stopToneN(note);
  }
}

void midi_volumeChanged(uint8_t volume){
  WMSynth.setMasterVolume(volume);
}

void midi_trackStart(void){
  SerialUSB.println("Track Start!");
}

void midi_trackEnd(void){
  SerialUSB.println("Track End!");
  WMSynth.stopAllTones();
}

void midi_setTicksPerNote(uint16_t ticksPerNote){
  SerialUSB.print("Setting ticks per note: ");
  SerialUSB.println(ticksPerNote);
}

void midi_setBPM(uint32_t bpm){
  SerialUSB.print("Setting bpm: ");
  SerialUSB.println(bpm);
}

void MIDIStreamHandler(int clientSocket){
  MidiParser parser = MidiParser(&midiCallbacks);
  uint8_t buf[BUFSIZE];

  // listen for incoming events
  while(1){
    memset(&buf, 0, BUFSIZE);
    int n = lwip_read(clientSocket, buf, BUFSIZE);
    if(n > 0){
      for(int i=0; i < n; i++){
        parser.feed(buf[i]);
      }
    } else {
      SerialUSB.println("No more events...");
      break;
    }
  }
  
  // close socket after everything is done
  SerialUSB.println("Closing client socket...");
  lwip_close(clientSocket);
  SerialUSB.println("Socket closed... cleaning task");

  if(MIDI_VERBOSE)
  {
      SerialUSB.print("total: ");
      SerialUSB.print(note_count);
      SerialUSB.print(", error: ");
      SerialUSB.print(error_count);
      SerialUSB.print(", error time: ");
      SerialUSB.print(error_total);
      SerialUSB.println("ms.\n");
  }
  
  note_count = 0;
  error_count = 0;
  error_total = 0;
  

}

void MIDIServer(void *arg){
  struct sockaddr_in serverAddr;  
  struct sockaddr_in clientAddr;
  socklen_t socklen;
  memset(&serverAddr, 0, sizeof(serverAddr));
  memset(&serverAddr, 0, sizeof(clientAddr));
  
  serverAddr.sin_len = sizeof(serverAddr);
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(MIDI_PORT);
  serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  
  int s = lwip_socket(AF_INET, SOCK_STREAM, 0);
  int err = lwip_bind(s, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
  if( err != 0 ){
    SerialUSB.println("Failed to bind to server socket!!!");
    assert(false);
  }

  err = lwip_listen(s, 1); // max number of connections is 1
  if( err != 0 ){
    SerialUSB.println("Failed to listen on server socket!!!");
    assert(false);
  }
  
  while(1){
    // wait for incoming connections
    int clientFd = lwip_accept(s, (struct sockaddr *)&clientAddr, &socklen);

    // get the details
    char str[INET_LEN];
    // now get it back and print it
    inet_ntop(AF_INET, &(clientAddr.sin_addr), str, INET_LEN);
    SerialUSB.print("Client: ");
    SerialUSB.print(str);
    SerialUSB.print(":");
    SerialUSB.println(ntohs(clientAddr.sin_port));

    MIDIStreamHandler(clientFd);
  }
  
  // close socket after everything is done
  lwip_close(s);
  SerialUSB.println("Server socket closed...");
}

/*
 * NTP Related functions
 */

void print_time(String tag, DueTime_t *t){
  SerialUSB.print(tag);
  SerialUSB.print(": ");
  SerialUSB.print(t->sec);
  SerialUSB.print("s ");
  SerialUSB.print(t->usec);
  SerialUSB.println("us");
}

void calculate_offset_and_delay(DueTime_t *t0, DueTime_t *t1, DueTime_t *t2, DueTime_t *t3, int64_t *offset, int64_t *delay){
  uint64_t xt0 = ((uint64_t)t0->sec)*MICROSECONDS + (t0->usec);
  uint64_t xt1 = ((uint64_t)t1->sec)*MICROSECONDS + (t1->usec);
  uint64_t xt2 = ((uint64_t)t2->sec)*MICROSECONDS + (t2->usec);
  uint64_t xt3 = ((uint64_t)t3->sec)*MICROSECONDS + (t3->usec);

  *offset = (int64_t)((xt1 - xt0) + (xt2 - xt3))/2;
  *delay =  (int64_t)((xt3 - xt0) + (xt2 - xt1))/2;
}

void ntp_receive_task(void *arg){
  NtpPacket_t packet;
  DueTime_t t0, t1, t2, t3;
  int64_t ntpOffset, ntpDelay;
  struct sockaddr clientAddr;
  socklen_t sockLen;

  ntp_build_socket();
  xTaskCreate(ntp_request_task, "ntp_req", configMINIMAL_STACK_SIZE*5, NULL, 2, NULL);
  
  while(1){
    memset(&packet, 0, SNTP_MSG_LEN);
    lwip_recvfrom(ntp_get_socket(), &packet, SNTP_MSG_LEN, 0, &clientAddr, &sockLen);
    Clock.getTime(&t3);
    
    ntp_get_time_from_packet(&packet, &t0, &t1, &t2);
   
    calculate_offset_and_delay(&t0, &t1, &t2, &t3, &ntpOffset, &ntpDelay);
    if(NTP_VERBOSE)
        print_time("Server time", &t2);
    // adjust clock only if delay below threshold
    if(ntpDelay <= NTP_MAX_DELAY)
    {
        if(abs(ntpOffset) < NTP_MAX_OFFSET && adjust == 1)
        {
            SerialUSB.println("Clock adjusted");
            SerialUSB.print("Add imposed offset: ");
            SerialUSB.println(IMPOSED_OFFSET);

            adjust = 0;
            Clock.addOffset((ntpOffset + IMPOSED_OFFSET));
            interval = 60 * 1000;
        }

        if(adjust == 1)
            Clock.addOffset(ntpOffset);

        if(NTP_VERBOSE)
        {
            SerialUSB.print("offset: ");
            SerialUSB.print((int32_t)ntpOffset);
            SerialUSB.print("us, delay: ");
            SerialUSB.print((int32_t)ntpDelay);
            SerialUSB.println("us");
        }
    }
    else
    {
        if(NTP_VERBOSE)
        {
            SerialUSB.print("offset: ");
            SerialUSB.print((int32_t)ntpOffset);
            SerialUSB.print("us, delay ");
            SerialUSB.print((int32_t)ntpDelay);
            SerialUSB.println("us. Discarded.");
        }
    }
  }
}

void ntp_send_request(void){
  NtpPacket_t packet;
  DueTime_t t0;
  //SerialUSB.println("sending ntp request...");
  Clock.getTime(&t0);
  ntp_get_request(&t0, &packet);
  lwip_sendto(ntp_get_socket(),
              &packet,
              SNTP_MSG_LEN,
              0,
              (struct sockaddr*)ntp_get_server(),
              sizeof(struct sockaddr_in)
            );
  //print_time("Sent t0", &t0);
  
  //SerialUSB.println("ntp request sent...\n");
}

void ntp_request_task(void *arg){ 
  interval = NTP_INTERVAL;
  while(1){
    ntp_send_request();
    vTaskDelay(pdMS_TO_TICKS(interval));
  }
}

/*
 * Start up routines
 */

void onError(int errorCode){
  SerialUSB.print("Error received: ");
  SerialUSB.println(errorCode);
}

void onReady(){
  SerialUSB.println("Device ready");  
  SerialUSB.print("Device IP: ");
  SerialUSB.println(IPAddress(PowerDueWiFi.getDeviceIP()));

  WMSynth.startSynth(3);
  WMSynth.stopAllTones();
  WMSynth.setMasterVolume(127);

  Clock.init();
  Clock.start();
  //SerialUSB.print(Clock.getCycleTime());
  
  xTaskCreate(ntp_receive_task, "ntp_recv", configMINIMAL_STACK_SIZE*5, NULL, 1, NULL);
  xTaskCreate(MIDIServer, "MIDIServer", configMINIMAL_STACK_SIZE*10, NULL, 1, NULL);
}

void setup() {
  PowerDue.LED();
  PowerDue.LED(PD_OFF);

  midiCallbacks.HandleEvent = midi_handleEvent;
  midiCallbacks.VolumeChanged = midi_volumeChanged;
  midiCallbacks.TrackStart = midi_trackStart;
  midiCallbacks.TrackEnd = midi_trackEnd;
  midiCallbacks.SetTicksPerNote = midi_setTicksPerNote;
  midiCallbacks.SetBPM = midi_setBPM;

  // enable export of clock on A4
  PIO_Configure(PIOA, PIO_PERIPH_A, PIO_PA2A_TIOA1, PIO_DEFAULT);

  SerialUSB.begin(0);
  while(!SerialUSB);

  PowerDueWiFi.init(WIFI_SSID, WIFI_PASS);
  PowerDueWiFi.setCallbacks(onReady, onError);
   
  vTaskStartScheduler();
  SerialUSB.println("Insufficient RAM");
  while(1);
}

void loop() {
  // not used
}
