#include "ntp.h"

#include <Arduino.h>
#include <assert.h>

static int __ntpSocket = -1;
static struct sockaddr_in _ntpServer;

void ntp_build_socket(void){
  if(__ntpSocket < 0){
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_len = sizeof(addr);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(NTP_PORT);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    int sock = lwip_socket(AF_INET, SOCK_DGRAM, 0);
    if(lwip_bind(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0){
      SerialUSB.println("Failed to bind ntp socket :(");
      while(1);
    }
    // define ntp server addr
    memset(&_ntpServer, 0, sizeof(_ntpServer));
    _ntpServer.sin_len = sizeof(_ntpServer);
    _ntpServer.sin_family = AF_INET;
    _ntpServer.sin_port = htons(NTP_PORT);
    inet_pton(AF_INET, NTP_SERVER, &(_ntpServer.sin_addr));
    
    __ntpSocket = sock;
  }
}

int ntp_get_socket(void){
  if(__ntpSocket < 0){
    ntp_build_socket();
  }
  return __ntpSocket;
}

struct sockaddr_in *ntp_get_server(void){
  return &_ntpServer;
}

// stuff due time into request packet
void ntp_get_request(DueTime_t *t0, NtpPacket_t *packet){
  memset(packet, 0, SNTP_MSG_LEN);
  
  packet->li_vn_mode = SNTP_LI_NO_WARNING | SNTP_VERSION | SNTP_MODE_CLIENT;
  
  // would you want to add in other info here?
  
  // place t0 into transmit_timestamp
  packet->transmit_timestamp[0] = lwip_htonl(t0->sec + DIFF_SEC_1900_1970);
  packet->transmit_timestamp[1] = lwip_htonl(USEC_TO_FRAC(t0->usec));
}

void ntp_get_time_from_packet(NtpPacket_t *packet, DueTime_t *t0, DueTime_t *t1, DueTime_t *t2){
  t0->sec = lwip_ntohl(packet->originate_timestamp[0]) - DIFF_SEC_1900_1970;
  t0->usec = FRAC_TO_USEC(lwip_ntohl(packet->originate_timestamp[1]));
  
  t1->sec = lwip_ntohl(packet->receive_timestamp[0]) - DIFF_SEC_1900_1970;
  t1->usec = FRAC_TO_USEC(lwip_ntohl(packet->receive_timestamp[1]));
  
  t2->sec = lwip_ntohl(packet->transmit_timestamp[0]) - DIFF_SEC_1900_1970;
  t2->usec = FRAC_TO_USEC(lwip_ntohl(packet->transmit_timestamp[1]));
}
