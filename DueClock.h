#ifndef __CLOCK_H
#define __CLOCK_H

#if defined(_SAM3XA_)

#include "DueTime.h"

class DueClock {
public:
  void init();
  
  void start();
  void stop();
  void reset();
  
  void getTime(DueTime_t *t);

  uint64_t millis();
  uint64_t micros();

  void addOffset(int64_t offset);

  int32_t getPendingOffset();
  void clearPendingOffset();

  uint32_t getCycleTime();

private:
  int32_t sOffset;
  int32_t pendingOffset;
  int32_t cycleTimeAdjust;
};

extern DueClock Clock;

#else

#error "Are you compiling for the PowerDue?"

#endif

#endif
