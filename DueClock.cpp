#include <Arduino.h>

#if defined(_SAM3XA_)

#include "PowerDue.h"
#include "DueClock.h"
#include <assert.h>

#define MICROSECOND 1000000
#define SECOND      1
#define CYCLE_TIME  (VARIANT_MCK/128)

#define TC_Module TC0   // TC0, TC1 or TC2

struct TCTimer {
  Tc *tc;
  uint32_t channel;     // 0, 1, or 2
  IRQn_Type irq;
};

const TCTimer nsCounter = {
  TC_Module, 2, TC2_IRQn
};

const TCTimer usCounter = {
  TC_Module, 1, TC1_IRQn
};

const TCTimer sCounter = {
  TC_Module, 0, TC0_IRQn
};

void DueClock::init(){
  pmc_set_writeprotect(false);

  /* set up the microsecond counter */
  // enable the peripheral clock
  pmc_enable_periph_clk(usCounter.irq);
  // configure the channel mode register: waveform mode
  TC_Configure(usCounter.tc, usCounter.channel,
    TC_CMR_WAVE |                   // TC waveform mode
    TC_CMR_WAVSEL_UP_RC |           // count up, resets when reaching RC value
    TC_CMR_TCCLKS_TIMER_CLOCK4 |    // which oscillator to use?
    TC_CMR_ACPA_CLEAR |             // set TIOA on TC = RA
    TC_CMR_ACPC_SET |               // clear TIOA on TC = RC
    TC_CMR_ASWTRG_SET |             // clear TIOA on software reset
    TC_CMR_BCPB_CLEAR |             // set TIOB on TC = RB
    TC_CMR_BCPC_SET |               // clear TIOB on TC = RC
    TC_CMR_BSWTRG_SET               // clear TIOB on software reset
  );
  // set RA, RB and RC values
  // RC value for 1 s
  uint32_t usrc = CYCLE_TIME;
  TC_SetRC(usCounter.tc, usCounter.channel, usrc);
  TC_SetRA(usCounter.tc, usCounter.channel, usrc/2);
  TC_SetRB(usCounter.tc, usCounter.channel, usrc/2);
  
  // Enable the RA and RC Compare Interrupt...
	usCounter.tc->TC_CHANNEL[usCounter.channel].TC_IER= (TC_IER_CPAS | TC_IER_CPCS);
	// ... and disable all others.
  usCounter.tc->TC_CHANNEL[usCounter.channel].TC_IDR=~(TC_IER_CPAS | TC_IER_CPCS);

  NVIC_ClearPendingIRQ(usCounter.irq);
  NVIC_EnableIRQ(usCounter.irq);
  
  /* set up the second counter */
  // enable the peripheral clock
  pmc_enable_periph_clk(sCounter.irq);
  // configure the channel mode register: waveform mode
  TC_Configure(sCounter.tc, sCounter.channel,
    TC_CMR_WAVE |                   // TC waveform mode
    TC_CMR_WAVSEL_UP |              // count up until 2^32
    TC_CMR_TCCLKS_XC0               // use XC0, set up for TIOA1, for chaining to usecond counter
  );
  TC_Module->TC_BMR = TC_BMR_TC0XC0S_TIOA1 | TC_BMR_TC1XC1S_TIOA2;

  cycleTimeAdjust = 0;
}

void DueClock::start(){
  TC_Start(sCounter.tc, sCounter.channel);
  TC_Start(usCounter.tc, usCounter.channel);
}

void DueClock::stop(){
  TC_Stop(sCounter.tc, sCounter.channel);
  TC_Stop(usCounter.tc, usCounter.channel);
}

void DueClock::reset(){
  this->start(); // calling start again will reset counters to zero
}

void DueClock::getTime(DueTime_t *t){
  assert(t != NULL);
  t->sec = (TC_ReadCV(sCounter.tc, sCounter.channel) + sOffset);
  t->usec = ( ((uint64_t)TC_ReadCV(usCounter.tc, usCounter.channel) * MICROSECOND) / this->getCycleTime());
}

uint64_t DueClock::micros(){
  return ( ((uint64_t)TC_ReadCV(usCounter.tc, usCounter.channel) * MICROSECOND) / this->getCycleTime() ) 
  + (uint64_t)(TC_ReadCV(sCounter.tc, sCounter.channel) + sOffset)*MICROSECOND;
}

uint64_t DueClock::millis(){
  return this->micros()/1000;
}

void DueClock::addOffset(int64_t offset){
    int32_t offset_s = (int32_t)(offset/MICROSECOND);
    sOffset += offset_s;

    // convert delay from us to ticks
    int32_t offset_us = (int32_t) (offset % MICROSECOND);
    pendingOffset = offset_us;
    int32_t diff_tick = (int32_t)((double)offset_us * ((double)this->getCycleTime() / MICROSECOND));

    /*
    SerialUSB.print("Diff_ticks = ");
    SerialUSB.println(diff_tick);
    */

    /* adjust phase by shifting cycle length */
    // Find the closest way to align the phase
    // For example, +0.90 -> +1.00 - 0.10; +0.40 -> +0.00 + 0.40;
    // In another word, try to move the phase for less than half of a cycle 
    /*
    if(abs(diff_tick) < (this->getCycleTime() / 2)){
        // Case 1: the difference is less than half of a cycle
        // if diff_tick is positive, ref clock has a larger clock number
        // we need to end a cycle earlier to catch up with the ref
        // which leads to subtracting Rc by diff_ticks.
        // the negative case works similarly
        pendingOffset =  this->getCycleTime() - diff_tick;
    }
    else{
        // Case 2: the difference is more than half of a cycle
        // then we'll shift it the other way, and adjust _seconds value accordingly
        // basically, diff_ticks > 0.5 cycle -> (cycle - diff_ticks) < 0.5 cycle
        // then  move it the other way: 2cycle - diff_ticks
        pendingOffset = (this->getCycleTime() - diff_tick + this->getCycleTime());
        if(offset > 0)
            sOffset++;
        else
            sOffset--;
    }
    */
    /*
    SerialUSB.print("Adjust Rc = ");
    SerialUSB.println(adjust_Rc);
    */

    /* Compliment RC is calculated here, but actual adjustment happens later, next time in RC intterupt */
}

int32_t DueClock::getPendingOffset(){
  return pendingOffset;
}

void DueClock::clearPendingOffset(){
  pendingOffset = 0;
}

uint32_t DueClock::getCycleTime(){
  return CYCLE_TIME;
}

DueClock Clock = DueClock();

void TC1_Handler(void){
  uint32_t status = TC_GetStatus(TC0, 1);
  // adjust RA and RC by pending offset
  if(status & TC_SR_CPAS){
    PowerDue.LED(PD_OFF);
  } else if(status & TC_SR_CPCS){
    PowerDue.LED(PD_BLUE);
    uint32_t usrc = (uint32_t) (Clock.getCycleTime() - ((double)Clock.getPendingOffset() * ((double)Clock.getCycleTime() / MICROSECOND)));
    TC_SetRC(usCounter.tc, usCounter.channel, usrc);
    TC_SetRA(usCounter.tc, usCounter.channel, usrc/2);
    /*
    if (Clock.getPendingOffset() != 0)
    {
        SerialUSB.print("PendingOffset: ");
        SerialUSB.print(Clock.getPendingOffset());
        SerialUSB.print(" Adjust RC = ");
        SerialUSB.println(usrc);
        SerialUSB.println();
    }
    */
    Clock.clearPendingOffset();
  }
}

#endif
